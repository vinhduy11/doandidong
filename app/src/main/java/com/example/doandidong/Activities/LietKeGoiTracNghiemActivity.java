package com.example.doandidong.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doandidong.Data.ChuDe;
import com.example.doandidong.Helper.CheckConnection;
import com.example.doandidong.Helper.SessionHelper;
import com.example.doandidong.Models.ChuDeModel;
import com.example.doandidong.R;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;

public class LietKeGoiTracNghiemActivity extends AppCompatActivity {

    LinearLayout layoutMain;
    LinearLayout layoutContent;
    TextView txtTitle;
    ChuDeModel chuDeModel;
    SessionHelper sessionHelper;

    private static final String TAG = LietKeGoiTracNghiemActivity.class.getSimpleName();

    //Checking wifi connection
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            CheckConnection ck = new CheckConnection();
            ck.checkConnecttion(context,intent);
        }
    };
    //------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liet_ke_goi_trac_nghiem);

        addControls();
        veTitle();
        veButtonChuDe();
    }

    private void veTitle() {
        try{
            sessionHelper = new SessionHelper(this);
            if (!sessionHelper.checkLogin()) {
                Intent intent = new Intent(LietKeGoiTracNghiemActivity.this, DangNhapActivity.class);
                startActivity(intent);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        txtTitle.setTypeface(null, Typeface.BOLD);
        txtTitle.setAllCaps(true);
        txtTitle.setTextSize(24);
        txtTitle.setText("Chủ đề");
    }

    private void addControls() {
        layoutMain = findViewById(R.id.layoutMain);
        layoutContent = findViewById(R.id.layoutContent);
        txtTitle = findViewById(R.id.txtTitle);
        chuDeModel = new ChuDeModel(this);
    }

    private void veButtonChuDe(){


        try {
            HashMap<String, ChuDe> chudes = chuDeModel.layChuDe();
            if (chudes!= null && chudes.size() > 0) {
                for (String key: chudes.keySet()) {

                    final Button btnChude = new Button(this);
                    btnChude.setText(chudes.get(key).getTenchude());
                    btnChude.setTag(key);
                    btnChude.setTypeface(Typeface.DEFAULT_BOLD);
                    btnChude.setTextSize(20);
                    btnChude.setTextColor(Color.parseColor("#ffffff"));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT
                    );
                    params.setMargins(0, 12, 0, 0);
                    btnChude.setLayoutParams(params);
                    btnChude.setBackgroundResource(R.drawable.chude_button_color);

                    btnChude.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        String chude = (String) btnChude.getTag();
                        Intent intent = new Intent(LietKeGoiTracNghiemActivity.this, LamTracNghiemActivity.class);
                        intent.putExtra("chude", chude);
                        startActivity(intent);

                        }
                    });
                    layoutContent.addView(btnChude);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());

        }
    }


    public void backtoMainActivity(View view) {
        Intent intent = new Intent(this, TrangChinhActivity.class);
        startActivity(intent);
    }

    //Lắng nghe wifi khi activity còn running
    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter=new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiver,filter);
    }
    //Ngừng lắng nghe khi activity stop
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }
}
