package com.example.doandidong.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.doandidong.Adapter.ThanhVienAdapter;
import com.example.doandidong.Data.NguoiDung;
import com.example.doandidong.Data.ThanhVien;
import com.example.doandidong.Helper.CheckConnection;
import com.example.doandidong.Helper.SessionHelper;
import com.example.doandidong.R;

public class ThongTinUngDungActivity extends AppCompatActivity {
    private static final String TAG = ThongTinUngDungActivity.class.getSimpleName();
    ListView lvDanhSachThanhVien;
    ThanhVien thanhVien;
    ThanhVienAdapter thanhVienAdapter;
    SessionHelper sessionHelper;

    //Checking wifi connection
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            CheckConnection ck = new CheckConnection();
            ck.checkConnecttion(context,intent);
        }
    };
    //------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thong_tin_ung_dung);
        addControls();
        addEvents();
    }

    private void addEvents() {
        lvDanhSachThanhVien.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                thanhVien = thanhVienAdapter.getItem(i);
            }
        });
        lvDanhSachThanhVien.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                thanhVien = thanhVienAdapter.getItem(i);
                return false;
            }
        });
    }

    private void addControls() {
        try{
            sessionHelper = new SessionHelper(this);
            if (!sessionHelper.checkLogin()) {
                Intent intent = new Intent(ThongTinUngDungActivity.this, DangNhapActivity.class);
                startActivity(intent);
            }
        }catch (Exception ex){
            Log.e(TAG, ex.toString());
        }
        lvDanhSachThanhVien = findViewById(R.id.lvDanhSachThanhVien);
        thanhVienAdapter = new ThanhVienAdapter(this,R.layout.about_danhsachthanhvien);
        lvDanhSachThanhVien.setAdapter(thanhVienAdapter);

        ThanhVien DinhVinhDuy = new ThanhVien("Đinh Vĩnh Duy",1416060003,"0938938577","vinhduy11@gmail.com");
        ThanhVien BuiAnhDung = new ThanhVien("Bùi Anh Dũng",1615061002,"0888196192","buianhdung60192@gmail.com");
        ThanhVien NguyenThanhDang = new ThanhVien("Nguyễn Thanh Đặng",1715061004,"0916082855","dangnt1507@gmail.com");
        ThanhVien TranTrungHieu = new ThanhVien("Trần Trung Hiếu",1615061015,"0355240746","trantrunghieu010692@gmail.com");
        ThanhVien TrinhXuanKhang = new ThanhVien("Trịnh Xuân Khang",1715061010,"0934762810","xuankhang@tvoq1hcm.edu.vn");
        ThanhVien LuuVinhThanh = new ThanhVien("Lưu Vĩnh Thành",1716061006,"0938370481","luuvinhthanh1997@gmail.com");
        ThanhVien VuongHieu = new ThanhVien("Vương Hiếu",1715060005,"0909050392","hieu.vuong1122@gmail.com");

        thanhVienAdapter.add(DinhVinhDuy); thanhVienAdapter.add(BuiAnhDung); thanhVienAdapter.add(NguyenThanhDang);
        thanhVienAdapter.add(TranTrungHieu); thanhVienAdapter.add(TrinhXuanKhang); thanhVienAdapter.add(LuuVinhThanh);
        thanhVienAdapter.add(VuongHieu);

        registerForContextMenu(lvDanhSachThanhVien);
    }

    //Lắng nghe wifi khi activity còn running
    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter=new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiver,filter);
    }
    //Ngừng lắng nghe khi activity stop
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.menu_thongtinungdung,menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==R.id.mnuCall){
            AlertDialog.Builder builder = new AlertDialog.Builder(ThongTinUngDungActivity.this);
            builder.setTitle("Make Call!");
            builder.setMessage("Bạn muốn có muốn gọi điện cho "+thanhVien.getTen()+" ?");
            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + thanhVien.getPhone()));

                    startActivity(intent);
                }
            });
            builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            });
            builder.create().show();

        }else if(item.getItemId()==R.id.mnuSMS){
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("sms:" + thanhVien.getPhone()));

            startActivity(intent);

        }
        return super.onContextItemSelected(item);
    }
}
