package com.example.doandidong.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.doandidong.Helper.CheckConnection;
import com.example.doandidong.Helper.SessionHelper;
import com.example.doandidong.R;

import java.util.HashMap;

public class KetQuaActivity extends AppCompatActivity {
    TableLayout tblKetqua;
    TextView txtKetQua;
    Intent intent;
    SessionHelper sessionHelper;
    private static final String TAG = KetQuaActivity.class.getSimpleName();

    //Checking wifi connection
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            CheckConnection ck = new CheckConnection();
            ck.checkConnecttion(context,intent);
        }
    };
    //------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ket_qua);
        addControls();
    }

    private void addControls() {
        try{
            sessionHelper = new SessionHelper(this);
            if (!sessionHelper.checkLogin()) {
                Intent intent = new Intent(KetQuaActivity.this, DangNhapActivity.class);
                startActivity(intent);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        tblKetqua = findViewById(R.id.tblKetqua);
        txtKetQua = findViewById(R.id.txtKetQua);
        intent = getIntent();
        loadKetQua();
    }


    private void loadKetQua() {
        HashMap<String, Integer> dapans = new HashMap<>();
        dapans = (HashMap<String, Integer>) intent.getSerializableExtra("dapans");
        int i=0;
        int diem=0;
        try {
            for (String key: dapans.keySet()) {
                i++;
                TableRow row = new TableRow(this);
                row.setGravity(Gravity.CENTER | Gravity.LEFT);

                TextView textView_1 = new TextView(this);
                textView_1.setText("Câu hỏi "+i+": ");
                textView_1.setTypeface(null, Typeface.BOLD);
                textView_1.setGravity(Gravity.CENTER | Gravity.LEFT);
                textView_1.setAllCaps(true);
                textView_1.setTextSize(20);
                textView_1.setTextColor(Color.parseColor("#cc3300"));
                textView_1.setLineSpacing(1/2, 1/2);
                ImageView imageView = new ImageView(this);
                if (dapans.get(key) == 1) {
                    diem++;
                    imageView.setImageResource(R.drawable.correct);
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                } else {
                    imageView.setImageResource(R.drawable.incorrect);
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                }
                row.addView(textView_1);
                row.addView(imageView);
                tblKetqua.addView(row);
                txtKetQua.setText(""+diem+"/"+dapans.size()+"");
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }


    }

    public void backtoMainActivity(View view) {
        Intent intent = new Intent(this, TrangChinhActivity.class);
        startActivity(intent);
    }

    //Lắng nghe wifi khi activity còn running
    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter=new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiver,filter);
    }
    //Ngừng lắng nghe khi activity stop
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }
}
