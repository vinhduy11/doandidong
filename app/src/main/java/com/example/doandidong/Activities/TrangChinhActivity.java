package com.example.doandidong.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Bundle;

import com.example.doandidong.Data.NguoiDung;
import com.example.doandidong.Helper.CheckConnection;
import com.example.doandidong.Helper.FireBaseHelper;
import com.example.doandidong.Helper.MyDBHandler;
import com.example.doandidong.Helper.SessionHelper;
import com.example.doandidong.R;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class TrangChinhActivity extends AppCompatActivity {
    private static final String TAG = TrangChinhActivity.class.getSimpleName();
    Button btnThoat;
    TextView txtName;
    NguoiDung user;
    private SessionHelper sessionHelper;
    private SQLiteDatabase db_reader;
    private SQLiteDatabase db_writer;
    private FireBaseHelper firebase;

    //Checking wifi connection
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            CheckConnection ck = new CheckConnection();
            ck.checkConnecttion(context,intent);
        }
    };
    //------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trang_chinh);
        //Create new Database for first Init
        try{
            MyDBHandler myDBHandler = new MyDBHandler(this);
            db_reader = myDBHandler.getReadableDatabase();
            db_writer = myDBHandler.getWritableDatabase();
            firebase = new FireBaseHelper(this);
            sessionHelper = new SessionHelper(this);
            user = sessionHelper.getLoginSession();
        }catch (Exception ex){
            Log.e(TAG, ex.toString());
        }//---------------------------------
        //Catch info User from LoginActivity

        addControls();
        registerForContextMenu(btnThoat);
        addEvents();
    }

    private void addEvents() {
        btnThoat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnThoat.showContextMenu();
            }
        });
    }

    private void addControls() {
        btnThoat= findViewById(R.id.btnThoat);
        txtName= findViewById(R.id.txtName);
        txtName.setText(user.getHoten());
    }

    public void xuLyMo_LietKeGoiTracNghiemActivity(View view) {
        Intent LietkeGoiTracNghiemActivity = new Intent(TrangChinhActivity.this, LietKeGoiTracNghiemActivity.class);
        startActivity(LietkeGoiTracNghiemActivity);
    }


    public void xuLyMo_ThongTinUngDungActivity(View view) {
        Intent ThongTinUngDung = new Intent(TrangChinhActivity.this,ThongTinUngDungActivity.class);
        startActivity(ThongTinUngDung);
    }

//    public void xuLyThoat(View view) {
//        AlertDialog.Builder builder =new AlertDialog.Builder(TrangChinhActivity.this);
//        builder.setTitle("Đăng xuất!");
//        builder.setMessage("Bạn muốn đăng xuất và thoát chương trình?");
//        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                sessionHelper.clearLoginSession();
//
//                if (!sessionHelper.checkLogin()) {
//                    Intent intent = new Intent(TrangChinhActivity.this, DangNhapActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(intent);
//                }
//            }
//        });
//        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.cancel();
//            }
//        });
//        builder.create().show();
//    }

    public void xuLyMo_ThongTinNguoiDungActivity(View view) {
        Intent ThongTinNguoiDung = new Intent(TrangChinhActivity.this,ThongTinNguoiDungActivity.class);
        startActivity(ThongTinNguoiDung);
    }

    public void xuLyDongBoDataFireBase(View view) {
        AlertDialog.Builder builder =new AlertDialog.Builder(TrangChinhActivity.this);
        builder.setTitle("Đồng bộ dữ liệu từ máy chủ ");
        builder.setMessage("Bạn có muốn tải về dữ liệu câu hỏi mới nhất không?");
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                firebase.syncCauHoiFromFireBase();
                firebase.syncChudeFromFireBase();
                Toast.makeText(TrangChinhActivity.this, "Đã sync dữ liệu từ máy chủ thành công!", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.create().show();
    }

    //Lắng nghe wifi khi activity còn running
    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter=new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiver,filter);
    }
    //Ngừng lắng nghe khi activity stop
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    //Tạo Menu cho nút thoát và nút đăng xuất - Cần có TITLE của project mới hiển thị

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_trangchinh,menu);
//        return super.onCreateOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//        if(item.getItemId()==R.id.mnuLogout){
//            AlertDialog.Builder builder =new AlertDialog.Builder(TrangChinhActivity.this);
//            builder.setTitle("Đăng xuất!");
//            builder.setMessage("Bạn muốn đăng xuất và thoát chương trình?");
//            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    sessionHelper.clearLoginSession();
//
//                    if (!sessionHelper.checkLogin()) {
//                        Intent intent = new Intent(TrangChinhActivity.this, DangNhapActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(intent);
//                    }
//                }
//            });
//            builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    dialogInterface.cancel();
//                }
//            });
//            builder.create().show();
//        }else if(item.getItemId()==R.id.mnuExit){
//            AlertDialog.Builder builder =new AlertDialog.Builder(TrangChinhActivity.this);
//            builder.setTitle("Đăng xuất!");
//            builder.setMessage("Bạn muốn đăng xuất và thoát chương trình?");
//            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    finish();
//                }
//            });
//            builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialogInterface, int i) {
//                    dialogInterface.cancel();
//                }
//            });
//            builder.create().show();
//        }
//        return super.onOptionsItemSelected(item);
//    }

    //Tạo Context Menu cho nut thoát
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.menu_trangchinh,menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==R.id.mnuLogout){
            AlertDialog.Builder builder =new AlertDialog.Builder(TrangChinhActivity.this);
            builder.setTitle("Đăng xuất!");
            builder.setMessage("Bạn muốn đăng xuất?");
            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    sessionHelper.clearLoginSession();

                    if (!sessionHelper.checkLogin()) {
                        Intent intent = new Intent(TrangChinhActivity.this, DangNhapActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }
            });
            builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            });
            builder.create().show();
        }else if(item.getItemId()==R.id.mnuExit){
            AlertDialog.Builder builder =new AlertDialog.Builder(TrangChinhActivity.this);
            builder.setTitle("Đăng xuất!");
            builder.setMessage("Bạn muốn đăng xuất và thoát chương trình?");
            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finishAffinity();
                }
            });
            builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            });
            builder.create().show();
        }
        return super.onContextItemSelected(item);
    }
}
