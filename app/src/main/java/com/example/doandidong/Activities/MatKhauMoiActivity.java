package com.example.doandidong.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.doandidong.Data.NguoiDung;
import com.example.doandidong.Helper.CheckConnection;
import com.example.doandidong.Helper.HashHelper;
import com.example.doandidong.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MatKhauMoiActivity extends AppCompatActivity {
    EditText edtPass, edtRepass;
    private HashHelper hashHelper;
    private static FirebaseDatabase database;
    DatabaseReference ref;
    private static final String TAG = MatKhauMoiActivity.class.getSimpleName();

    //Checking wifi connection
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            CheckConnection ck = new CheckConnection();
            ck.checkConnecttion(context,intent);
        }
    };
    //------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mat_khau_moi);
        addControls();
        addEvents();
    }

    private void addEvents() {
        try {
            database = FirebaseDatabase.getInstance();
            hashHelper = new HashHelper();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    private void addControls() {
        edtPass = findViewById(R.id.edtPass);
        edtRepass = findViewById(R.id.edtRepass);
        edtPass.setGravity(Gravity.CENTER);
        edtRepass.setGravity(Gravity.CENTER);
    }

    public void xuLyCapNhat(View view) {

        if (edtPass.getText().length() == 0 || edtRepass.getText().length() == 0) {
            Toast.makeText(this, "Vui lòng không được bỏ trống mật khẩu hay xác nhận mật khẩu!", Toast.LENGTH_SHORT).show();
        } else {
            if (edtPass.getText().toString().equals(edtRepass.getText().toString())) {
                Intent intent = getIntent();
                final String hashEmail = intent.getStringExtra("hashEmail");
                ref = database.getReference().child("nguoidung").child(hashEmail);
                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            NguoiDung nguoiDung = dataSnapshot.getValue(NguoiDung.class);
                            dataSnapshot.getRef().removeValue();
                            nguoiDung.setMatkhau(edtPass.getText().toString());
                            dataSnapshot.getRef().setValue(nguoiDung);
                            Toast.makeText(MatKhauMoiActivity.this, "Đổi mật khẩu thành công!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(MatKhauMoiActivity.this, DangNhapActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(MatKhauMoiActivity.this, "Đã có lỗi trong quá trình xử lý!Vui lòng thử lại.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            } else {
                Toast.makeText(MatKhauMoiActivity.this, "Mật khẩu và xác nhận mật khẩu không giống nhau!Vui lòng kiểm tra.", Toast.LENGTH_SHORT).show();
            }

        }
    }

    //Lắng nghe wifi khi activity còn running
    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter=new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiver,filter);
    }
    //Ngừng lắng nghe khi activity stop
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }
}
