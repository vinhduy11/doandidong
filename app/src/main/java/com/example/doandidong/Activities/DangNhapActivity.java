package com.example.doandidong.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.doandidong.Data.NguoiDung;
import com.example.doandidong.Helper.CheckConnection;
import com.example.doandidong.Helper.HashHelper;
import com.example.doandidong.Helper.SessionHelper;
import com.example.doandidong.Models.NguoiDungModel;
import com.example.doandidong.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DangNhapActivity extends AppCompatActivity {
    NguoiDungModel nguoiDungModel;
    private static final String TAG = DangKyActivity.class.getSimpleName();
    EditText edtEmail,edtMatKhau;

    private static FirebaseDatabase database;
    DatabaseReference ref;
    private SessionHelper sessionHelper;

    //Checking wifi connection
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            CheckConnection ck = new CheckConnection();
            ck.checkConnecttion(context,intent);
        }
    };
    //------------------------


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dang_nhap);
        addControls();
    }


    private void addControls() {
        edtEmail=findViewById(R.id.edtEmail);
        edtMatKhau=findViewById(R.id.edtPass);
        edtEmail.setGravity(Gravity.CENTER);
        edtMatKhau.setGravity(Gravity.CENTER);

        try {
            database = FirebaseDatabase.getInstance();
            nguoiDungModel = new NguoiDungModel(this);
            sessionHelper = new SessionHelper(this);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    public void moTrangDangKy(View view) {
        Intent intent = new Intent(DangNhapActivity.this, DangKyActivity.class);
        startActivity(intent);
    }

    public void xuLyDangNhap(View view) {

        if(edtEmail.getText().length()!=0 && edtMatKhau.getText().length()!=0)
        {
            HashHelper hashHelper = new HashHelper();
            String hash = hashHelper.StringtoHash(edtEmail.getText().toString());
            ref = database.getReference().child("nguoidung").child(hash);
            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (!dataSnapshot.exists()){
                        Toast.makeText(DangNhapActivity.this, "Người dùng không tồn tại!! Vui lòng kiểm tra.", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        NguoiDung nguoiDung = dataSnapshot.getValue(NguoiDung.class);
                        String email = nguoiDung.getEmail();
                        if (!edtMatKhau.getText().toString().equals(nguoiDung.getMatkhau())){
                            Toast.makeText(DangNhapActivity.this,"Mật khẩu đăng nhập không đúng! Vui lòng kiểm tra.",Toast.LENGTH_LONG).show();
                        }
                        else {
                            sessionHelper.updateLoginSession(nguoiDung);
                            Intent intent = new Intent(DangNhapActivity.this, TrangChinhActivity.class);
                            startActivity(intent);
                        }

                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
        else
        {
            Toast.makeText(DangNhapActivity.this,"Bạn chưa nhập đủ thông tin",Toast.LENGTH_LONG).show();
        }
    }

    public void forgetPasswordPage(View view) {
        Intent intent = new Intent(DangNhapActivity.this, CapNhatMatKhauActivity.class);
        startActivity(intent);
    }

    //Lắng nghe wifi khi activity còn running
    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter=new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiver,filter);
    }
    //Ngừng lắng nghe khi activity stop
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }
}
