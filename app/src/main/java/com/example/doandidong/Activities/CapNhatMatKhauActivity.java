package com.example.doandidong.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.doandidong.Data.NguoiDung;
import com.example.doandidong.Helper.CheckConnection;
import com.example.doandidong.Helper.HashHelper;
import com.example.doandidong.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class CapNhatMatKhauActivity extends AppCompatActivity {
    EditText edtEmail;
    private HashHelper hashHelper;
    private static FirebaseDatabase database;
    DatabaseReference ref;
    private static final String TAG = CapNhatMatKhauActivity.class.getSimpleName();

    //Checking wifi connection
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            CheckConnection ck = new CheckConnection();
            ck.checkConnecttion(context,intent);
        }
    };
    //------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cap_nhat_mat_khau);
        addControls();
        addEvents();
    }

    private void addEvents() {
        try {
            database = FirebaseDatabase.getInstance();
            hashHelper = new HashHelper();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    private void addControls() {
        edtEmail = findViewById(R.id.edtEmail);
        edtEmail.setGravity(Gravity.CENTER);
    }



    public void xuLyTiepTuc(View view) {
        if (edtEmail.getText().length() == 0) {
            Toast.makeText(this, "Vui lòng nhập email!", Toast.LENGTH_SHORT).show();
        } else {
            final String hashEmail = hashHelper.StringtoHash(edtEmail.getText().toString());

            ref = database.getReference().child("nguoidung").child(hashEmail);
            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        Intent intent = new Intent(CapNhatMatKhauActivity.this, MatKhauMoiActivity.class);
                        intent.putExtra("hashEmail", hashEmail);
                        startActivity(intent);
                    } else {
                        Toast.makeText(CapNhatMatKhauActivity.this, "Email không tồn tại! Vui lòng kiểm tra.", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    //Lắng nghe wifi khi activity còn running
    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter=new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiver,filter);
    }
    //Ngừng lắng nghe khi activity stop
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }
}
