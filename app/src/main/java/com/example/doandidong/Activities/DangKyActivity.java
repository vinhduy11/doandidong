package com.example.doandidong.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doandidong.Data.NguoiDung;
import com.example.doandidong.Helper.CheckConnection;
import com.example.doandidong.Helper.HashHelper;
import com.example.doandidong.Models.NguoiDungModel;
import com.example.doandidong.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DangKyActivity extends AppCompatActivity implements View.OnClickListener{
    Button btnDangKy;
    TextView txtThoat;
    NguoiDungModel nguoiDungModel;
    private static final String TAG = DangKyActivity.class.getSimpleName();
    EditText edtHoTen,edtEmail,edtPhone,edtPass,edtRepass;
    private static FirebaseDatabase database;
    DatabaseReference ref;
    private static HashHelper hashHelper = new HashHelper();

    //Checking wifi connection
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            CheckConnection ck = new CheckConnection();
            ck.checkConnecttion(context,intent);
        }
    };
    //------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dang_ky);
        addControls();
        addEvents();
    }

    private void addEvents() {
        btnDangKy.setOnClickListener(this);
        txtThoat.setOnClickListener(this);
    }

    private void addControls() {
        edtHoTen=findViewById(R.id.edtHoten);
        edtEmail=findViewById(R.id.edtEmail);
        edtPhone=findViewById(R.id.edtPhone);
        edtPass=findViewById(R.id.edtPass);
        edtRepass=findViewById(R.id.edtRepass);
        btnDangKy=findViewById(R.id.btnDangKy);
        txtThoat=findViewById(R.id.txtThoat);

        try {
            nguoiDungModel = new NguoiDungModel(this);
            database = FirebaseDatabase.getInstance();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /*public void xuLyDangKy(View view) {
        String email = edtEmail.getText().toString().trim();
        String emailPattern = "^[A-Za-z0-9+_.-]+@(.+)$";

        if (
            edtHoTen.getText().length()  ==0  ||
            edtEmail.getText().length()   ==0  ||
            edtRepass.getText().length() == 0 ||
            edtPass.getText().length()   == 0
        )
        {
            Toast.makeText(DangKyActivity.this,"Không được bỏ trống những dòng có dấu sao!",Toast.LENGTH_LONG).show();
        }
        else if (!email.matches(emailPattern))
        {
            Toast.makeText(getApplicationContext(),"Email không đúng định dạng",Toast.LENGTH_SHORT).show();
        }

        else if (!edtPass.getText().toString().equals(edtRepass.getText().toString()))
        {
            Toast.makeText(DangKyActivity.this,"Mật khẩu và xác nhận mật khẩu phải giống nhau!",Toast.LENGTH_LONG).show();
        }
        else {
            HashHelper hashHelper = new HashHelper();
            final String hash = hashHelper.StringtoHash(edtEmail.getText().toString());
            ref = database.getReference().child("nguoidung");
            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    NguoiDung nd = dataSnapshot.child(hash).getValue(NguoiDung.class);
                    if (nd != null) {
                        Toast.makeText(DangKyActivity.this, "Email đã tồn tại", Toast.LENGTH_SHORT).show();
                    } else {
                        NguoiDung nguoiDung = new NguoiDung();
                        nguoiDung.setEmail(edtEmail.getText().toString());
                        nguoiDung.setMatkhau(edtPass.getText().toString());
                        nguoiDung.setHoten(edtHoTen.getText().toString());
                        nguoiDung.setPhone(edtPhone.getText().toString());
                        dataSnapshot.child(hash).getRef().setValue(nguoiDung);
                        Toast.makeText(DangKyActivity.this,"Đăng ký thành công!",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(DangKyActivity.this, DangNhapActivity.class);
                        startActivity(intent);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }

    }*/


    /*public void xuLyThoat(View view) {
        Intent intent = new Intent(DangKyActivity.this, DangNhapActivity.class);
        startActivity(intent);
    }*/

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btnDangKy)
        {
            String email = edtEmail.getText().toString().trim();
            String emailPattern = "^[A-Za-z0-9+_.-]+@(.+)$";

            if (
                    edtHoTen.getText().length()  ==0  ||
                            edtEmail.getText().length()   ==0  ||
                            edtRepass.getText().length() == 0 ||
                            edtPass.getText().length()   == 0
            )
            {
                Toast.makeText(DangKyActivity.this,"Không được bỏ trống những dòng có dấu sao!",Toast.LENGTH_LONG).show();
            }
            else if (!email.matches(emailPattern))
            {
                Toast.makeText(getApplicationContext(),"Email không đúng định dạng",Toast.LENGTH_SHORT).show();
            }

            else if (!edtPass.getText().toString().equals(edtRepass.getText().toString()))
            {
                Toast.makeText(DangKyActivity.this,"Mật khẩu và xác nhận mật khẩu phải giống nhau!",Toast.LENGTH_LONG).show();
            }
            else {
                HashHelper hashHelper = new HashHelper();
                final String hash = hashHelper.StringtoHash(edtEmail.getText().toString());
                ref = database.getReference().child("nguoidung");
                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        NguoiDung nd = dataSnapshot.child(hash).getValue(NguoiDung.class);
                        if (nd != null) {
                            Toast.makeText(DangKyActivity.this, "Email đã tồn tại", Toast.LENGTH_SHORT).show();
                        } else {
                            NguoiDung nguoiDung = new NguoiDung();
                            nguoiDung.setEmail(edtEmail.getText().toString());
                            nguoiDung.setMatkhau(edtPass.getText().toString());
                            nguoiDung.setHoten(edtHoTen.getText().toString());
                            nguoiDung.setPhone(edtPhone.getText().toString());
                            dataSnapshot.child(hash).getRef().setValue(nguoiDung);
                            Toast.makeText(DangKyActivity.this,"Đăng ký thành công!",Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(DangKyActivity.this, DangNhapActivity.class);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        }
        else if(v.getId()==R.id.txtThoat)
        {
            Intent intent = new Intent(DangKyActivity.this, DangNhapActivity.class);
            startActivity(intent);
        }
    }

    //Lắng nghe wifi khi activity còn running
    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter=new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiver,filter);
    }
    //Ngừng lắng nghe khi activity stop
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }
}
