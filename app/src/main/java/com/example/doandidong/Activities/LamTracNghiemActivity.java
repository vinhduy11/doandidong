package com.example.doandidong.Activities;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doandidong.Data.CauHoi;
import com.example.doandidong.Data.NguoiDung;
import com.example.doandidong.Helper.CheckConnection;
import com.example.doandidong.Helper.MyDBHandler;
import com.example.doandidong.Helper.SessionHelper;
import com.example.doandidong.Models.LamTracNghiemModel;
import com.example.doandidong.R;

import java.util.HashMap;

public class LamTracNghiemActivity extends AppCompatActivity {
    LinearLayout layoutContent;
    Intent intent;
    TextView txtTitle;
    HashMap<String, CauHoi> cauHois;
    Button btnNopBai;
    LamTracNghiemModel lamTracNghiemModel;
    private SessionHelper sessionHelper;
    NguoiDung user;

    //Lay ten Class de ghi log
    private static final String TAG = LamTracNghiemActivity.class.getSimpleName();

    //Checking wifi connection
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            CheckConnection ck = new CheckConnection();
            ck.checkConnecttion(context,intent);
        }
    };
    //------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_lam_trac_nghiem);
        addControls();
    }

    private void addControls() {
        try {
            MyDBHandler myDBHandler = new MyDBHandler(this);
            sessionHelper = new SessionHelper(this);
            user = sessionHelper.getLoginSession();
            if (!sessionHelper.checkLogin()) {
                Intent intent = new Intent(LamTracNghiemActivity.this, DangNhapActivity.class);
                startActivity(intent);
            }else {
                user = sessionHelper.getLoginSession();
            }
        } catch (Exception ex){
            Log.e(TAG, ex.toString());
        }
        intent = getIntent();
        layoutContent = findViewById(R.id.layoutContent);
        txtTitle = findViewById(R.id.txtTitle);
        renderTitle();
        String chude = (String) intent.getStringExtra("chude");
        loadCauhoi(chude);
        if (cauHois != null && cauHois.size()>0) {
            renderCauHoi();
        }

        renderSaveButton();
    }

    private void renderTitle() {
        txtTitle.setTypeface(null, Typeface.BOLD);
        txtTitle.setText("Câu hỏi");
        txtTitle.setAllCaps(true);
        txtTitle.setTextSize(24);
    }

    private void renderSaveButton() {
        btnNopBai = new Button(this);
        btnNopBai.setText("Lưu kết quả");
        btnNopBai.setBackgroundResource(R.drawable.chude_button_color);
        btnNopBai.setTextColor(Color.parseColor("#FFFFFF"));
        btnNopBai.setAllCaps(true);
        btnNopBai.setTextSize(18);
        btnNopBai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            try {
                if ( cauHois!= null && cauHois.size() > 0) {
                    HashMap<String, Integer> dapans = new HashMap<>();

                    for (String i: cauHois.keySet()) {
                        RadioGroup btnGroupId = layoutContent.findViewWithTag("radGroup_"+cauHois.get(i).getCauhoi_id());
                        if (btnGroupId.getCheckedRadioButtonId() != -1)
                        {
                            String dapan = ((RadioButton)findViewById(btnGroupId.getCheckedRadioButtonId())).getText().toString().charAt(0)+"";
                            if (dapan.equals(cauHois.get(i).getDapan())) {
                                dapans.put(cauHois.get(i).getCauhoi_id(), 1);
                            } else {
                                dapans.put(cauHois.get(i).getCauhoi_id(), 0);
                            }

                        }
                    }

                    if (dapans.size() < cauHois.size()) {

                        // setup the alert builder
                        AlertDialog.Builder builder = new AlertDialog.Builder(LamTracNghiemActivity.this);
                        builder.setTitle("Chú ý!");
                        builder.setMessage("Vui lòng làm hết câu hỏi trước khi lưu! Bạn mới làm "+dapans.size()+" câu trong "+cauHois.size()+" câu.");
                        // add a button
                        builder.setNegativeButton("OK", null);

                        AlertDialog dialog = builder.create();
                        dialog.setCancelable(false);
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.show();
                    } else {
                        luuBai(dapans);
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }


            }
        });

        layoutContent.addView(btnNopBai);
    }


    @SuppressLint("ResourceType")
    private void renderCauHoi() {
        int so = 0;
        for (String i: cauHois.keySet()) {

            so++;
            TextView cauhoiTitle = new TextView(this);
            cauhoiTitle.setTypeface(null, Typeface.BOLD);
            cauhoiTitle.setGravity(7);
            cauhoiTitle.setAllCaps(true);
            cauhoiTitle.setTextSize(18);

            cauhoiTitle.setTextColor(Color.parseColor("#cc6600"));
            cauhoiTitle.setText("Câu hỏi "+so+"."+cauHois.get(i).getNoidungcauhoi());

            RadioGroup radioGroup = new RadioGroup(this);

            radioGroup.setTag("radGroup_"+cauHois.get(i).getCauhoi_id());
            RadioButton radioButton_1 = new RadioButton(this);
            radioButton_1.setTextSize(16);
            radioButton_1.setText("A. " +cauHois.get(i).getNoidungtraloi_1());
            radioButton_1.setId(1);

            RadioButton radioButton_2 = new RadioButton(this);
            radioButton_2.setTextSize(16);
            radioButton_2.setText("B. " +cauHois.get(i).getNoidungtraloi_2());
            radioButton_2.setId(2);

            RadioButton radioButton_3 = new RadioButton(this);
            radioButton_3.setTextSize(16);
            radioButton_3.setText("C. " +cauHois.get(i).getNoidungtraloi_3());
            radioButton_3.setId(3);

            RadioButton radioButton_4 = new RadioButton(this);
            radioButton_4.setTextSize(16);
            radioButton_4.setText("D. " +cauHois.get(i).getNoidungtraloi_4());
            radioButton_4.setId(4);

            radioGroup.addView(radioButton_1);
            radioGroup.addView(radioButton_2);
            radioGroup.addView(radioButton_3);
            radioGroup.addView(radioButton_4);

            layoutContent.addView(cauhoiTitle);
            layoutContent.addView(radioGroup);
        }
        TextView txtView = new TextView(getBaseContext());
        layoutContent.addView(txtView);
        TextView txtView1 = new TextView(getBaseContext());
        layoutContent.addView(txtView1);

    }

    private void loadCauhoi(String chude) {
        lamTracNghiemModel = new LamTracNghiemModel(this.getBaseContext());
        if (lamTracNghiemModel != null) {
            cauHois = lamTracNghiemModel.layCauHoiTracNghiem(chude);
        }

    }

    private void luuBai(final HashMap<String, Integer> dapans) {
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Xác nhận!");
        builder.setMessage("Bạn có chắc muốn lưu bài hay không?");
        // add a button
        builder.setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            Long results = lamTracNghiemModel.luuBai(user, dapans);
            if (results > 0) {
                Toast.makeText(LamTracNghiemActivity.this, "Lưu bài thành công!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(LamTracNghiemActivity.this, KetQuaActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("ketquaId", results);
                intent.putExtra("dapans", dapans);
                startActivity(intent);
            } else {
                Toast.makeText(LamTracNghiemActivity.this, "Lưu bài thất bại!", Toast.LENGTH_SHORT).show();
            }

            }
        });
        builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        // create and show the alert dialog

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    //Lắng nghe wifi khi activity còn running
    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter=new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiver,filter);
    }
    //Ngừng lắng nghe khi activity stop
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }
}
