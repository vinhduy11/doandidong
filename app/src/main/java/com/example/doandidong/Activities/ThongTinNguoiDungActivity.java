package com.example.doandidong.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.doandidong.Data.NguoiDung;
import com.example.doandidong.Helper.CheckConnection;
import com.example.doandidong.Helper.FireBaseHelper;
import com.example.doandidong.Helper.HashHelper;
import com.example.doandidong.Helper.SessionHelper;
import com.example.doandidong.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ThrowOnExtraProperties;
import com.google.firebase.database.ValueEventListener;

public class ThongTinNguoiDungActivity extends AppCompatActivity {
    EditText edtTen, edtPhone;
    SessionHelper sessionHelper;
    NguoiDung user;
    private static final String TAG = ThongTinNguoiDungActivity.class.getSimpleName();
    private static FirebaseDatabase firebase;
    private DatabaseReference ref;

    //Checking wifi connection
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            CheckConnection ck = new CheckConnection();
            ck.checkConnecttion(context,intent);
        }
    };
    //------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thong_tin_nguoi_dung);

        try {
            sessionHelper = new SessionHelper(this);
            if (!sessionHelper.checkLogin()) {
                Intent intent = new Intent(ThongTinNguoiDungActivity.this, DangNhapActivity.class);
                startActivity(intent);
            } else {
                user = sessionHelper.getLoginSession();
            }

            firebase = FirebaseDatabase.getInstance();
            ref = firebase.getReference().child("nguoidung");
            addControls();

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    private void addControls() {
        edtTen = findViewById(R.id.edtTen);
        edtPhone = findViewById(R.id.edtPhone);
        edtTen.setText(user.getHoten());
        edtPhone.setText(user.getPhone());
    }

    public void saveInfos(View view) {
        Boolean checked = false;
        String msg="";
        if (edtTen.getText().toString().equals("")) {
            msg = "Tên không được để trống! ";
        }
        else {
            checked = true;
        }


        if (checked == false) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        } else {
            NguoiDung nguoiDung = new NguoiDung();
            nguoiDung.setHoten(edtTen.getText().toString());
            nguoiDung.setEmail(user.getEmail());
            nguoiDung.setPhone(edtPhone.getText().toString());
            nguoiDung.setMatkhau(user.getMatkhau());

            final NguoiDung u = nguoiDung;
            HashHelper hashHelper = new HashHelper();
            String hash = hashHelper.StringtoHash(user.getEmail());
            ref.child(hash).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        dataSnapshot.getRef().removeValue();
                        dataSnapshot.getRef().setValue(u);
                        String msg ="Cập nhật thông tin thành công!";
                        Toast.makeText(ThongTinNguoiDungActivity.this, msg, Toast.LENGTH_SHORT).show();
                        sessionHelper.updateLoginSession(u);
                        Intent intent = new Intent(ThongTinNguoiDungActivity.this, TrangChinhActivity.class);
                        startActivity(intent);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }

    }

    public void xuLyThoat(View view) {
        Intent intent = new Intent(ThongTinNguoiDungActivity.this, TrangChinhActivity.class);
        startActivity(intent);
    }

    //Lắng nghe wifi khi activity còn running
    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter=new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiver,filter);
    }
    //Ngừng lắng nghe khi activity stop
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }
}
