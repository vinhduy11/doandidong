package com.example.doandidong.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.doandidong.Data.ThanhVien;
import com.example.doandidong.R;

public class ThanhVienAdapter extends ArrayAdapter<ThanhVien> {
    Activity context;
    int resource;
    public ThanhVienAdapter(@NonNull Activity context, int resource) {
        super(context, resource);
        this.context=context;
        this.resource=resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View custom =context.getLayoutInflater().inflate(resource, null);
        ThanhVien tv =getItem(position);
        ImageView imgAvatar = custom.findViewById(R.id.imgAvatar);
        TextView txtTen = custom.findViewById(R.id.txtTen);
        TextView txtMssv = custom.findViewById(R.id.txtMssv);
        TextView txtPhone = custom.findViewById(R.id.txtPhone);
        TextView txtEmail = custom.findViewById(R.id.txtEmail);

        txtTen.setText(tv.getTen());
        txtMssv.setText(tv.getMssv()+"");
        txtPhone.setText(tv.getPhone());
        txtEmail.setText(tv.getEmail());

        return custom;
    }
}
