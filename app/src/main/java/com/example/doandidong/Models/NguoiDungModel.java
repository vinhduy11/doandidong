package com.example.doandidong.Models;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.doandidong.Data.NguoiDung;
import com.example.doandidong.Helper.MyDBHandler;

import java.util.ArrayList;
import java.util.HashMap;


public class NguoiDungModel {
    private Context context;
    private MyDBHandler myDBHandler;

    //Get Connection from DB
    private SQLiteDatabase db_reader;
    private SQLiteDatabase db_writer;
    private Cursor cursor;

    //Lay ten Class de ghi log
    private static final String TAG = NguoiDungModel.class.getSimpleName();

    public NguoiDungModel(Context context) {
        this.context = context;
        try {
            myDBHandler = new MyDBHandler(context);
            db_reader = myDBHandler.getReadableDatabase();
            db_writer = myDBHandler.getWritableDatabase();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    public NguoiDung dangnhap(String Email, String Matkhau) {
     NguoiDung nguoiDung=null;
        try {
            Cursor cursor = null;
            //Tao cursor lay query tai day. VD o day cau query la select * from chude
            cursor =  db_reader.rawQuery("SELECT * FROM nguoidung WHERE email = Email AND matkhau=Matkhau", null);

            // Neu cursor co du lieu > 0 thi....
            if (cursor.getCount() > 0)
                return nguoiDung;
            else
                return null;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        return  nguoiDung;
    }

    public int kiemtranguoidungtontai(String Email) {
        int kq=0;
        try {
            Cursor cursor = null;
            //Tao cursor lay query tai day. VD o day cau query la select * from chude
            cursor =  db_reader.rawQuery("SELECT * FROM nguoidung WHERE email = Email", null);

            // Neu cursor co du lieu > 0 thi....
            if (cursor.getCount() > 0)
                return 1;
            else
                return 0;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        return kq;
    }

    public NguoiDung layThongTinNguoiDung(String email) {
          NguoiDung nguoiDung = null;

        try {
            Cursor cursor = null;
            //Tao cursor lay query tai day. VD o day cau query la select * from chude
            cursor =  db_reader.rawQuery("SELECT * FROM nguoidung", null);

            // Neu cursor co du lieu > 0 thi....
            if (cursor.getCount() > 0)
            {
                Log.i(TAG, "Load Câu hỏi từ DB");
                //Bat dau tu du lieu thu nhat
                cursor.moveToFirst();
                do {
                    nguoiDung.setEmail(cursor.getString(cursor.getColumnIndex("email")));
                    nguoiDung.setMatkhau(cursor.getString(cursor.getColumnIndex("matkhau")));
                    nguoiDung.setHoten(cursor.getString(cursor.getColumnIndex("hoten")));
                    nguoiDung.setPhone(cursor.getString(cursor.getColumnIndex("phone")));
                } while (cursor.moveToNext());

            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
            return nguoiDung;
        }
        return  nguoiDung;
    }

    public Long themthongtinnguoidung(NguoiDung nguoidung){
        Long kq=null;
        try {
            if (nguoidung != null ) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("email", nguoidung.getEmail());
                contentValues.put("matkhau", nguoidung.getMatkhau());
                contentValues.put("hoten", nguoidung.getHoten());
                contentValues.put("phone", nguoidung.getPhone());

                //Neu du duoc ghi vao thi #0 con khong thi tra ra 0
                kq = db_writer.insert("nguoidung", null, contentValues);
            }
            return kq;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            return kq;
        }
    }

    public int suathongtinnguoidung(int id,String Email,String Matkhau,String Hoten,String Phone){
        int kq=0;
        try {
            Cursor cursor = null;
            //Tao cursor lay query tai day. VD o day cau query la select * from chude
            cursor =  db_writer.rawQuery("UPDATE nguoidung SET email = Email,matkhau=Matkhau,hoten=Hoten,phone=Phone WHERE user_id=id", null);

            // Neu cursor co du lieu > 0 thi....
            if (cursor.getCount() > 0)
                return 1;
            else
                return 0;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        return kq;
    }
    public int capnhatmatkhau(String Email,String Matkhau){
        int kq=0;
        try {
            Cursor cursor = null;
            //Tao cursor lay query tai day. VD o day cau query la select * from chude
            cursor =  db_reader.rawQuery("UPDATE nguoidung SET matkhau=Matkhau WHERE email=Email", null);

            // Neu cursor co du lieu > 0 thi....
            if (cursor.getCount() > 0)
                return 1;
            else
                return 0;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        return kq;
    }

}
