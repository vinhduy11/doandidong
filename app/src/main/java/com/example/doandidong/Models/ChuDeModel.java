package com.example.doandidong.Models;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.doandidong.Data.ChuDe;
import com.example.doandidong.Helper.MyDBHandler;

import java.util.HashMap;

public class ChuDeModel {

    private Context context;
    private MyDBHandler myDBHandler;

    //Get Connection from DB
    private SQLiteDatabase db_reader;
    private SQLiteDatabase db_writer;
    private Cursor cursor;

    //Lay ten Class de ghi log
    private static final String TAG = ChuDeModel.class.getSimpleName();
    public ChuDeModel(Context context) {
        this.context = context;
        try {
            myDBHandler = new MyDBHandler(context);
            db_reader = myDBHandler.getReadableDatabase();
            db_writer = myDBHandler.getWritableDatabase();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    public HashMap<String, ChuDe> layChuDe() {
        HashMap<String, ChuDe> chuDeHashMap= new HashMap<>();
        try {

            Cursor cursor = null;
            //Tao cursor lay query tai day. VD o day cau query la select * from chude
            cursor =  db_reader.rawQuery("SELECT * FROM chude", null);

            // Neu cursor co du lieu > 0 thi....
            if (cursor.getCount() > 0)
            {
                Log.i(TAG, "Load Câu hỏi từ DB");
                //Bat dau tu du lieu thu nhat
                cursor.moveToFirst();
                do {
                    ChuDe chuDe = new ChuDe();
                    chuDe.setChude_id(cursor.getString((cursor.getColumnIndex("chude_id"))));
                    chuDe.setTenchude(cursor.getString((cursor.getColumnIndex("tenchude"))));
                    chuDeHashMap.put(chuDe.getChude_id().toString(), chuDe);
                } while (cursor.moveToNext());

                return chuDeHashMap;
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
            return chuDeHashMap;
        }
        return chuDeHashMap;
    }


}
