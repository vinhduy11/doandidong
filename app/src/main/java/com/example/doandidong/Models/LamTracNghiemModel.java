package com.example.doandidong.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.doandidong.Data.CauHoi;
import com.example.doandidong.Data.NguoiDung;
import com.example.doandidong.Helper.FireBaseHelper;
import com.example.doandidong.Helper.HashHelper;
import com.example.doandidong.Helper.MyDBHandler;
import com.example.doandidong.Helper.SessionHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class LamTracNghiemModel {
    private Context context;
    private MyDBHandler myDBHandler;
    private final static int slCauhoi = 10;
    private HashHelper hashHelper = new HashHelper();

    //Get Connection from DB
    private SQLiteDatabase db_reader;
    private SQLiteDatabase db_writer;
    private Cursor cursor;

    //Lay ten Class de ghi log
    private static final String TAG = LamTracNghiemModel.class.getSimpleName();


    public LamTracNghiemModel(Context context) {
        this.context = context;
        try {
            myDBHandler = new MyDBHandler(context);
            db_reader = myDBHandler.getReadableDatabase();
            db_writer = myDBHandler.getWritableDatabase();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    public Long getKetQuaId() {
        long unixTime = System.currentTimeMillis() / 1000L;
        return unixTime;
    }

    public Long luuBai(NguoiDung nguoiDung, HashMap<String, Integer> dapan) {
        Long rs=null;
        try {
            if (dapan != null && dapan.size()> 0) {
                Long Id = getKetQuaId();
                for(String key: dapan.keySet()) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("ketqua_id", Id);
                    contentValues.put("cauhoi_id", key);
                    contentValues.put("nguoitraloi", nguoiDung.getEmail());
                    contentValues.put("dapan", dapan.get(key));
                    //Neu du duoc ghi vao thi #0 con khong thi tra ra 0
                    rs = db_writer.insert("ketqua", null, contentValues);
                }
                if (rs!=null && rs > 0) {
                    FireBaseHelper fireBaseHelper = new FireBaseHelper(this.context);
                    fireBaseHelper.insertKetquatoFireBase(nguoiDung, Id+"", dapan);
                }
                return Id;
            }
            return null;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            return null;
        }
    }

    public HashMap<String, CauHoi> layCauHoiTracNghiem(String chude) {
        Random rand = new Random();
        HashMap<String, CauHoi> baiTN = null;
        List<CauHoi> AllCauHois = null;

        try {
            baiTN = new HashMap<>();
            AllCauHois = new ArrayList<>();
            Cursor cursor = null;
            //Tao cursor lay query tai day. VD o day cau query la select * from chude
            cursor =  db_reader.rawQuery("SELECT * FROM cauhoi WHERE chude ='"+chude+"'", null);

            // Neu cursor co du lieu > 0 thi....
            if (cursor.getCount() > 0)
            {
                Log.i(TAG, "Load Câu hỏi từ DB");
                //Bat dau tu du lieu thu nhat
                cursor.moveToFirst();
                do {
                    CauHoi cauHoi = new CauHoi();
                    cauHoi.setCauhoi_id(String.valueOf(cursor.getInt((cursor.getColumnIndex("cauhoi_id")))));
                    cauHoi.setChude(cursor.getString((cursor.getColumnIndex("chude"))));
                    cauHoi.setNoidungcauhoi(cursor.getString((cursor.getColumnIndex("noidungcauhoi"))));
                    cauHoi.setNoidungtraloi_1(cursor.getString((cursor.getColumnIndex("noidungtraloi_1"))));
                    cauHoi.setNoidungtraloi_2(cursor.getString((cursor.getColumnIndex("noidungtraloi_2"))));
                    cauHoi.setNoidungtraloi_3(cursor.getString((cursor.getColumnIndex("noidungtraloi_3"))));
                    cauHoi.setNoidungtraloi_4(cursor.getString((cursor.getColumnIndex("noidungtraloi_4"))));
                    cauHoi.setDapan(cursor.getString((cursor.getColumnIndex("dapan"))));
                    AllCauHois.add(cauHoi);
                } while (cursor.moveToNext());
                //Tao bo cau hoi
                for (int i = 0; i < slCauhoi; i++) {
                    int randomIndex = rand.nextInt(AllCauHois.size());
                    CauHoi randomElement = AllCauHois.get(randomIndex);
                    if (!baiTN.containsKey(randomElement.getCauhoi_id())) {
                        baiTN.put(randomElement.getCauhoi_id(), randomElement);
                    } else {
                        i--;
                    }
                }
                return baiTN;
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
            return baiTN;
        }
        return baiTN;
    }
}
