package com.example.doandidong.Data;

import androidx.annotation.NonNull;

public class ChuDe {
    String chude_id;
    String tenchude;

    public ChuDe() {
    }

    public ChuDe(String chude_id, String tenchude) {
        this.chude_id = chude_id;
        this.tenchude = tenchude;
    }

    public String getChude_id() {
        return chude_id;
    }

    public void setChude_id(String chude_id) {
        this.chude_id = chude_id;
    }

    public String getTenchude() {
        return tenchude;
    }

    public void setTenchude(String tenchude) {
        this.tenchude = tenchude;
    }

    @NonNull
    @Override
    public String toString() {
        return tenchude;
    }
}
