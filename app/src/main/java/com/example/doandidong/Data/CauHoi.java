package com.example.doandidong.Data;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class CauHoi {
    String cauhoi_id;
    String chude;
    String noidungcauhoi;
    String noidungtraloi_1;
    String noidungtraloi_2;
    String noidungtraloi_3;
    String noidungtraloi_4;
    String dapan;

    public String getCauhoi_id() {
        return cauhoi_id;
    }

    public void setCauhoi_id(String cauhoi_id) {
        this.cauhoi_id = cauhoi_id;
    }

    public String getChude() {
        return chude;
    }

    public void setChude(String chude) {
        this.chude = chude;
    }

    public String getNoidungcauhoi() {
        return noidungcauhoi;
    }

    public void setNoidungcauhoi(String noidungcauhoi) {
        this.noidungcauhoi = noidungcauhoi;
    }

    public String getNoidungtraloi_1() {
        return noidungtraloi_1;
    }

    public void setNoidungtraloi_1(String noidungtraloi_1) {
        this.noidungtraloi_1 = noidungtraloi_1;
    }

    public String getNoidungtraloi_2() {
        return noidungtraloi_2;
    }

    public void setNoidungtraloi_2(String noidungtraloi_2) {
        this.noidungtraloi_2 = noidungtraloi_2;
    }

    public String getNoidungtraloi_3() {
        return noidungtraloi_3;
    }

    public void setNoidungtraloi_3(String noidungtraloi_3) {
        this.noidungtraloi_3 = noidungtraloi_3;
    }

    public String getNoidungtraloi_4() {
        return noidungtraloi_4;
    }

    public void setNoidungtraloi_4(String noidungtraloi_4) {
        this.noidungtraloi_4 = noidungtraloi_4;
    }

    public String getDapan() {
        return dapan;
    }

    public void setDapan(String dapan) {
        this.dapan = dapan;
    }
}
