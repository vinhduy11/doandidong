package com.example.doandidong.Data;

public class ThanhVien {
    private String ten;
    private int mssv;
    private String phone;
    private String email;

    public ThanhVien() {
    }

    public ThanhVien(String ten, int mssv, String phone, String email) {
        this.ten = ten;
        this.mssv = mssv;
        this.phone = phone;
        this.email = email;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public int getMssv() {
        return mssv;
    }

    public void setMssv(int mssv) {
        this.mssv = mssv;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
