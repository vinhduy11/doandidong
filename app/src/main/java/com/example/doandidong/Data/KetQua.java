package com.example.doandidong.Data;

import java.sql.Timestamp;

public class KetQua {
    String ketqua_id;
    Long cauhoi_id;
    String nguoitraloi;
    int dapan;
    Timestamp ngaytao;

    public KetQua() {
    }

    public KetQua(String ketqua_id, Long cauhoi_id, String nguoitraloi, int dapan, Timestamp ngaytao) {
        this.ketqua_id = ketqua_id;
        this.cauhoi_id = cauhoi_id;
        this.nguoitraloi = nguoitraloi;
        this.dapan = dapan;
        this.ngaytao = ngaytao;
    }

    public String getKetqua_id() {
        return ketqua_id;
    }

    public void setKetqua_id(String ketqua_id) {
        this.ketqua_id = ketqua_id;
    }

    public Long getCauhoi_id() {
        return cauhoi_id;
    }

    public void setCauhoi_id(Long cauhoi_id) {
        this.cauhoi_id = cauhoi_id;
    }

    public String getNguoitraloi() {
        return nguoitraloi;
    }

    public void setNguoitraloi(String nguoitraloi) {
        this.nguoitraloi = nguoitraloi;
    }

    public int getDapan() {
        return dapan;
    }

    public void setDapan(int dapan) {
        this.dapan = dapan;
    }

    public Timestamp getNgaytao() {
        return ngaytao;
    }

    public void setNgaytao(Timestamp ngaytao) {
        this.ngaytao = ngaytao;
    }
}
