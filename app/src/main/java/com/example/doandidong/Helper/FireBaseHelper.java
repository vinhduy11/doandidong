package com.example.doandidong.Helper;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.doandidong.Activities.ThongTinNguoiDungActivity;
import com.example.doandidong.Data.CauHoi;
import com.example.doandidong.Data.ChuDe;
import com.example.doandidong.Data.KetQua;
import com.example.doandidong.Data.NguoiDung;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FireBaseHelper {
    //OK
    private Context context;
    private MyDBHandler myDBHandler;
    private static FirebaseDatabase database;
    DatabaseReference ref;
    private static HashHelper hashHelper = new HashHelper();

    //Get Connection from DB
    private SQLiteDatabase db_reader;
    private SQLiteDatabase db_writer;
    private Cursor cursor;
    NguoiDung user;
    int rs=0;
    private SessionHelper sessionHelper;

    //Lay ten Class de ghi log
    private static final String TAG = FireBaseHelper.class.getSimpleName();


    public FireBaseHelper(Context context) {
        this.context = context;
        try {
            myDBHandler = new MyDBHandler(context);
            db_reader = myDBHandler.getReadableDatabase();
            db_writer = myDBHandler.getWritableDatabase();
            database = FirebaseDatabase.getInstance();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }

    //Sync Down to local
    public void syncChudeFromFireBase() {
        final long result=0;
        try {
            // Get a reference to our posts
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference ref = database.getReference().child("chude");
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    db_writer.execSQL("DELETE FROM chude");
                    for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                        ChuDe chuDe = childDataSnapshot.getValue(ChuDe.class);

                        ContentValues contentValues = new ContentValues();
                        contentValues.put("chude_id", chuDe.getChude_id());
                        contentValues.put("tenchude", chuDe.getTenchude());
                        db_writer.insert("chude", null, contentValues);
                    }
                }


                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
    public long syncCauHoiFromFireBase() {
        final long result=0;
        try {
            // Get a reference to our posts
            ref = database.getReference().child("cauhoi");
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    List<CauHoi> cauHoiList = new ArrayList<CauHoi>();
                    for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                        if (!childDataSnapshot.getKey().equals("0")) {
                            if (dataSnapshot.child(childDataSnapshot.getKey()).getValue() instanceof ArrayList) {
                                ArrayList<String> list = (ArrayList) dataSnapshot.child(childDataSnapshot.getKey()).getValue();
                                try {
                                    CauHoi cauHoi = new CauHoi();
                                    cauHoi.setCauhoi_id(String.valueOf(list.get(0)));
                                    cauHoi.setChude(list.get(1)+"");
                                    cauHoi.setNoidungcauhoi(list.get(2)+"");
                                    cauHoi.setNoidungtraloi_1(String.valueOf(list.get(3)));
                                    cauHoi.setNoidungtraloi_2(String.valueOf(list.get(4)));
                                    cauHoi.setNoidungtraloi_3(String.valueOf(list.get(5)));
                                    cauHoi.setNoidungtraloi_4(String.valueOf(list.get(6)));
                                    cauHoi.setDapan(list.get(7)+"");
                                    cauHoiList.add(cauHoi);
                                } catch (Exception e) {
                                    Log.e(TAG, e.toString());
                                }

                            }
                        }
                    }
                    db_writer.execSQL("DELETE FROM cauhoi");
                    insertCauhoiToLocalDB(cauHoiList);
                }


                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        return result;
    }

    private void insertCauhoiToLocalDB(List<CauHoi> cauHoiList) {
        for (int i=0; i<cauHoiList.size(); i++) {
            long result=0;
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("cauhoi_id", cauHoiList.get(i).getCauhoi_id());
                contentValues.put("chude", cauHoiList.get(i).getChude());
                contentValues.put("noidungcauhoi", cauHoiList.get(i).getNoidungcauhoi());
                contentValues.put("noidungtraloi_1", cauHoiList.get(i).getNoidungtraloi_1());
                contentValues.put("noidungtraloi_2", cauHoiList.get(i).getNoidungtraloi_2());
                contentValues.put("noidungtraloi_3", cauHoiList.get(i).getNoidungtraloi_3());
                contentValues.put("noidungtraloi_4", cauHoiList.get(i).getNoidungtraloi_4());
                contentValues.put("dapan", cauHoiList.get(i).getDapan());

                //Neu du duoc ghi vao thi #0 con khong thi tra ra 0
                result = db_writer.insert("cauhoi", null, contentValues);
            } catch (Exception e) {
                Log.e(TAG, e.toString());
            }
        }

    }

    //Push to FireBase
    public int insertUsertoFireBase(NguoiDung nguoidung) {
        String key = null;
        rs = 0;
        try {
            String hash = hashHelper.StringtoHash(nguoidung.getEmail());
            ref = database.getReference().child("nguoidung").child(hash);
            ref.setValue(nguoidung, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Log.e(TAG, "Data could not be saved " + databaseError.getMessage());
                }
                }
            });
            key = ref.getKey();

            if (key != null && key.equals(hash)) {
                rs = 1;
            }

            return rs;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            return rs;
        }
    }
    private void getResultCallback(int result) {
        rs = result;
    }

    public int insertKetquatoFireBase(NguoiDung user, String timehash, HashMap<String, Integer> dapan) {
        if (dapan != null && dapan.size()>0) {
            ArrayList<KetQua> ketQuas = new ArrayList<>();
            for (String key: dapan.keySet()) {
                KetQua ketQua = new KetQua();
                ketQua.setCauhoi_id(Long.parseLong(key));
                ketQua.setNguoitraloi(user.getEmail());
                ketQua.setDapan(dapan.get(key));
                ketQuas.add(ketQua);
            }
            String key = null;

            try {
                String userHash = hashHelper.StringtoHash(user.getEmail());
                ref = database.getReference().child("ketqua").child(userHash).child(timehash);
                ref.setValue(ketQuas, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                    if (databaseError != null) {
                        Log.e(TAG, "Data could not be saved " + databaseError.getMessage());
                    }
                    }
                });
                key = ref.getKey();

                if (key != null && key.equals(timehash)) {
                    rs = 1;
                }

                return rs;
            } catch (Exception e) {
                Log.e(TAG, e.toString());
                return rs;
            }
        }
        return rs;
    }

    //Check Data from FireBase

    public NguoiDung checkUserExistedonFireBase(NguoiDung nguoidung) {

        user= null;
        try {

            final String hash = hashHelper.StringtoHash(nguoidung.getEmail());

            ref = database.getReference().child("nguoidung");
            ref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    NguoiDung data = dataSnapshot.child(hash).getValue(NguoiDung.class);
                    if (data != null) {
                        checkUserExistedCallBack(data);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            return nguoidung;

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        return nguoidung;
    }

    private void checkUserExistedCallBack(NguoiDung u) {
        user = u;
    }


}
