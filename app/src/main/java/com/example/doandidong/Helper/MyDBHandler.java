package com.example.doandidong.Helper;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.NonNull;

import com.example.doandidong.Data.CauHoi;
import com.example.doandidong.Data.ChuDe;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

public class MyDBHandler extends SQLiteOpenHelper {
    private static final String TAG = MyDBHandler.class.getSimpleName();
    private final String dbPath;
    private final Context context;
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "doan.db";
    private static final String TABLE_CAUHOI = "cauhoi";

    public MyDBHandler(Context context) throws IOException {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        this.dbPath = "/data/data/"
                + context.getApplicationContext().getPackageName() + "/databases/"
                + DATABASE_NAME;
    }

    private void dropTableifExists(SQLiteDatabase db) {
        //Drop if exists
        Log.i(TAG, "Drop if exists Table for First Init");
        db.execSQL("DROP TABLE IF EXISTS nguoidung");
        db.execSQL("DROP TABLE IF EXISTS chude");
        db.execSQL("DROP TABLE IF EXISTS cauhoi");
        db.execSQL("DROP TABLE IF EXISTS ketqua");
    }

    private void createTable(SQLiteDatabase db) {
        //Create Table
        Log.i(TAG, "Create New Table for First Init");
        String createTableNguoiDung = "CREATE TABLE  nguoidung  ( " +
                "  email varchar PRIMARY KEY NOT NULL, " +
                "  matkhau  varchar NOT NULL, " +
                "  hoten  varchar NOT NULL, " +
                "  phone  varchar" +
                ")";

        String createTableChude = "CREATE TABLE  chude (" +
                "chude_id varchar PRIMARY KEY UNIQUE, " +
                "tenchude varchar NOT NULL" +
                ")";

        String createTableCauhoi = "CREATE TABLE  cauhoi (" +
                "cauhoi_id varchar PRIMARY KEY UNIQUE," +
                "chude varchar NOT NULL, " +
                "noidungcauhoi varchar NOT NULL," +
                "noidungtraloi_1 varchar NOT NULL," +
                "noidungtraloi_2 varchar NOT NULL," +
                "noidungtraloi_3 varchar NOT NULL," +
                "noidungtraloi_4 varchar NOT NULL," +
                "dapan varchar NOT NULL" +
                ")";

        String createTableKetQua = "CREATE TABLE  ketqua (" +
                "ketqua_id INTEGER NOT NULL, " +
                "cauhoi_id INTEGER NOT NULL, " +
                "nguoitraloi varchar NOT NULL, " +
                "dapan INTEGER NOT NULL DEFAULT 0, " +
                "ngaytao DATETIME DEFAULT CURRENT_TIMESTAMP" +
                ")";

        db.execSQL(createTableNguoiDung);
        db.execSQL(createTableChude);
        db.execSQL(createTableCauhoi);
        db.execSQL(createTableKetQua);
    }

    private void insertChuDe(SQLiteDatabase db) {
        final long result=0;
        try {
            Log.i(TAG, "Load Chu De from FireBase for First Init");
            // Get a reference to our posts
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference ref = database.getReference().child("chude");
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                        ChuDe chuDe = childDataSnapshot.getValue(ChuDe.class);

                        ContentValues contentValues = new ContentValues();
                        contentValues.put("chude_id", chuDe.getChude_id());
                        contentValues.put("tenchude", chuDe.getTenchude());

                        SQLiteDatabase database1 = MyDBHandler.this.getWritableDatabase();
                        database1.insert("chude", null, contentValues);
                    }
                }


                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void insertCauHoi(SQLiteDatabase db) {
        //Insert from FireBase
        final long result=0;
        try {
            Log.i(TAG, "Load Cau hoi from FireBase for First Init");
            // Get a reference to our posts
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference ref = database.getReference().child("cauhoi");
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    List<CauHoi> cauHoiList = new ArrayList<CauHoi>();
                    for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                        if (!childDataSnapshot.getKey().equals("0")) {
                            if (dataSnapshot.child(childDataSnapshot.getKey()).getValue() instanceof ArrayList) {
                                ArrayList<String> list = (ArrayList) dataSnapshot.child(childDataSnapshot.getKey()).getValue();
                                try {
                                    CauHoi cauHoi = new CauHoi();
                                    cauHoi.setCauhoi_id(String.valueOf(list.get(0)));
                                    cauHoi.setChude(list.get(1)+"");
                                    cauHoi.setNoidungcauhoi(list.get(2)+"");
                                    cauHoi.setNoidungtraloi_1(String.valueOf(list.get(3)));
                                    cauHoi.setNoidungtraloi_2(String.valueOf(list.get(4)));
                                    cauHoi.setNoidungtraloi_3(String.valueOf(list.get(5)));
                                    cauHoi.setNoidungtraloi_4(String.valueOf(list.get(6)));
                                    cauHoi.setDapan(list.get(7)+"");
                                    cauHoiList.add(cauHoi);
                                } catch (Exception e) {
                                    Log.e(TAG, e.toString());
                                }
                            }
                        }
                    }

                    for (int i=0; i<cauHoiList.size(); i++) {
                        long result=0;
                        try {
                            ContentValues contentValues = new ContentValues();
                            contentValues.put("cauhoi_id", cauHoiList.get(i).getCauhoi_id());
                            contentValues.put("chude", cauHoiList.get(i).getChude());
                            contentValues.put("noidungcauhoi", cauHoiList.get(i).getNoidungcauhoi());
                            contentValues.put("noidungtraloi_1", cauHoiList.get(i).getNoidungtraloi_1());
                            contentValues.put("noidungtraloi_2", cauHoiList.get(i).getNoidungtraloi_2());
                            contentValues.put("noidungtraloi_3", cauHoiList.get(i).getNoidungtraloi_3());
                            contentValues.put("noidungtraloi_4", cauHoiList.get(i).getNoidungtraloi_4());
                            contentValues.put("dapan", cauHoiList.get(i).getDapan());

                            SQLiteDatabase database1 = MyDBHandler.this.getWritableDatabase();
                            //Neu du duoc ghi vao thi #0 con khong thi tra ra 0
                            result = database1.insert("cauhoi", null, contentValues);
                        } catch (Exception e) {
                            Log.e(TAG, e.toString());
                        }
                    }
                }


                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(TAG, "Load Process Init DB for First Init");
        dropTableifExists(db);
        createTable(db);
        insertCauHoi(db);
        insertChuDe(db);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }



}
