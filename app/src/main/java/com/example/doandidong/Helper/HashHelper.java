package com.example.doandidong.Helper;

import android.util.Log;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public class HashHelper {
    private static final String TAG = HashHelper.class.getSimpleName();
    public String StringtoHash(String stringtohash) {
        String hash = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] hashInBytes = md.digest(stringtohash.getBytes(StandardCharsets.UTF_8));

            StringBuilder sb = new StringBuilder();
            for (byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }
            hash = sb.toString();
            System.out.println(sb.toString());
            return hash;
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            return hash;
        }
    }
}
