package com.example.doandidong.Helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.example.doandidong.Data.NguoiDung;

public class SessionHelper {
    private final String sessionKey = "sessions";
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;

    public SessionHelper(Activity activity) {
        sharedpreferences = activity.getSharedPreferences(sessionKey, Context.MODE_PRIVATE);
    }

    public void createLoginSession(NguoiDung nguoiDung) {
        editor = sharedpreferences.edit();
        String lastLoginTime = (System.currentTimeMillis() / 1000L)+"";
        editor.putString("name", nguoiDung.getHoten());
        editor.putString("phone", nguoiDung.getPhone());
        editor.putString("email", nguoiDung.getEmail());
        editor.putString("password", nguoiDung.getEmail());
        editor.putString("lastLoginTime", lastLoginTime);
        editor.commit();
    }

    public void updateLoginSession(NguoiDung nguoiDung) {
        editor = sharedpreferences.edit();
        editor.putString("name", nguoiDung.getHoten());
        editor.putString("phone", nguoiDung.getPhone());
        editor.putString("email", nguoiDung.getEmail());
        editor.putString("password", nguoiDung.getMatkhau());
        editor.commit();
    }

    public NguoiDung getLoginSession() {
        NguoiDung nguoiDung = new NguoiDung();
        nguoiDung.setHoten(sharedpreferences.getString("name", null));
        nguoiDung.setPhone(sharedpreferences.getString("phone", null));
        nguoiDung.setEmail(sharedpreferences.getString("email", null));
        nguoiDung.setMatkhau(sharedpreferences.getString("password", null));

        return nguoiDung;
    }

    public boolean checkLogin() {
        String name = sharedpreferences.getString("name", null);
        if (name != null) {
            return true;
        }
        return false;
    }

    public void clearLoginSession() {
        if(sharedpreferences!= null) {
            editor = sharedpreferences.edit();
            editor.clear();
            editor.commit();
        }
    }
}
