package com.example.doandidong.Helper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.widget.Toast;

import com.example.doandidong.Activities.NotificationActivity;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class CheckConnection {
    public void checkConnecttion(final Context context, Intent intent){
        ConnectivityManager connectivityManager=
                (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo= connectivityManager.getActiveNetworkInfo();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("MẤT KẾT NỐI MẠNG!");
        builder.setMessage("Bạn đang bị mất kết nối mạng, vui lòng kết nối lại!");
        builder.setCancelable(false);
        builder.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                context.startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
            }
        });

        AlertDialog alert = builder.create();

        if(networkInfo!=null){
            //Toast.makeText(context,"Has internet",Toast.LENGTH_LONG).show();
            if(alert!= null && alert.isShowing()) {
                alert.dismiss();
            }
        }else{
//            Intent i = new Intent(context, NotificationActivity.class);
//            context.startActivity(i);
            alert.show();
        }
    }
}
